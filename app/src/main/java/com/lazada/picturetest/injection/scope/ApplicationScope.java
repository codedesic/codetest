package com.lazada.picturetest.injection.scope;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by Steven O'Brien on 1/15/17
 */
@Scope
@Retention(RetentionPolicy.CLASS)
public @interface ApplicationScope {



}
