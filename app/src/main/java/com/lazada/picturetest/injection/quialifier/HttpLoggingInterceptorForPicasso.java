package com.lazada.picturetest.injection.quialifier;

import javax.inject.Qualifier;

/**
 * Created by Steven O'Brien on 1/1/17.
 */
@Qualifier
public @interface HttpLoggingInterceptorForPicasso {
}
