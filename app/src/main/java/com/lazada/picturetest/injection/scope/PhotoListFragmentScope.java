package com.lazada.picturetest.injection.scope;

import javax.inject.Scope;

/**
 * Created by Steven O'Brien on 1/16/17.
 */
@Scope
public @interface PhotoListFragmentScope {
}
